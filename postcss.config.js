module.exports = {
    plugins: [
        require('postcss-image-inliner'),
        require('postcss-cssnext'),
        require('css-mqpacker'),
        require('cssnano')({
            preset: [
                'default',
                {
                    discardComments: {
                        removeAll: true
                    }
                }
            ]
        })
    ]
}