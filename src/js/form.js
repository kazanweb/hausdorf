(function() {

	var form = function() {
		$('[data-form-label]').each(function() {

			if(this.value) {
				$(this).addClass('form__input_full');
			}

			$(this).blur(function() {
				if(this.value) {
					$(this).addClass('form__input_full');
				} else {
					$(this).removeClass('form__input_full');
				}
			});

		});
	}

	form();

	$(document).ajaxSuccess(function() {
		form();
	});

			// $('[type=tel]').inputmask('+7 (999) 999-99-99');

})();
