window.$ = require('jquery');
require('lazysizes');
require('jquery-match-height');
require('./plugins/tabs/js/tabs');
require('./swiper');
require('./swiper-large');
require('./app');
require('./form')
window.noUiSlider = require('noUiSlider');
require('./filter')
